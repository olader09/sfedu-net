Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post :user_token, to: 'user_token#create'

  resource :user

  resources :books, only: [:index, :show] do
    get :watched, on: :collection
    get :refresh, on: :collection
    get :favorites, on: :collection
    get :mark, on: :member
  end

  resources :films, only: [:index, :show] do
    get :watched, on: :collection
    get :refresh, on: :collection
    get :favorites, on: :collection
    get :mark, on: :member
  end



  # resources :watched_cards do
  #   post :add, on: :collection
  # end
  # resources :watched_films do
  #   post :add, on: :collection
  # end
end
