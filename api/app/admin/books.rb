ActiveAdmin.register Book do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :name, :author, :info, :img, :source, :countri
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :author, :info, :img, :source, :countri]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  index do 
    id_column
    column :name
    column :countri
    column :author
    column :info
    actions
  end
end
