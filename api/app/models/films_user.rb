class FilmsUser < ApplicationRecord
  belongs_to :user
  belongs_to :film


  def favorites!
    self.update_attributes(favorites: !self.favorites)
  end
end