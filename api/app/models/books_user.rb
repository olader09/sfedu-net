class BooksUser < ApplicationRecord
  belongs_to :user
  belongs_to :book

  def favorites!
    self.update_attributes(favorites: !self.favorites)
  end
end