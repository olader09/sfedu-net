# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :create, :read, :update, :destroy, to: :crud

    user ||= User.new

    case user.role
    when 'user'
      can :crud, User, id: user.id
      # can :crud, Card, user_id: user.id
    when 'admin'
      can :manage, User
      can :manage, Book
      can :manage, Film
      can :manage, BooksUser
      can :manage, FilmsUser
    end
  end
end
