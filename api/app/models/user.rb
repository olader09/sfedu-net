class User < ApplicationRecord
  extend Enumerize

  validates :username, presence: true, :uniqueness => { :case_sensitive => true }
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP } 
  has_secure_password
  # has_one :card, dependent: :destroy
  has_many :books_users
  has_many :books, through: :books_users

  has_many :films_users
  has_many :films, through: :films_users

  # has_many :shared_cards_with_me, through: :cards_users, source: :card

  enumerize :role, in: %i[user admin], default: :user, predicates: true, scope: :shallow

  def self.from_token_request(request)
    username = request.params&.[]('auth')&.[]('username')
    email = request.params&.[]('auth')&.[]('email')
    (find_by username: username) || (find_by email: username) || (find_by email: email) || (find_by username: email)
  end

  def favorite_books
    books_users.where(favorites: true)
  end

  def favorite_films
    films_users.where(favorites: true)
  end
end
