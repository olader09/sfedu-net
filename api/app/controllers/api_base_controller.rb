class APIBaseController < ActionController::API
  before_action :init_redis
  before_action :check_recommendations
  include Knock::Authenticable

  def check_recommendations
    return unless jwt.present?
    @redis.set(jwt_key(:books),[]) if @redis.get(jwt_key(:books)).nil?
    @redis.set(jwt_key(:films),[]) if @redis.get(jwt_key(:films)).nil?
  end
  
  protected

  def init_redis
    @redis = Redis.new(host: "redis", port: 6379, db: 15)
  end

  def jwt_key(type)
    "#{type}:{jwt}"
  end

  def jwt
    return unless request.headers[:Authorization].present?
    request.headers[:Authorization].gsub("Bearer ", "")
  end
end
