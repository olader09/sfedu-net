class FilmsController < APIBaseController
  before_action :authenticate_user


  def show
    film = Film.find(params[:id]) #Выбираем книгу из списка
    if current_user.films.where(id: film).present?
      render json: film
    else
      current_user.films << film  #Добавление юзеру в просмотренные
      render json: film
    end
  end

  def index  
    @watched = JSON.parse(@redis.get(jwt_key(:films)))
    taken_now = Film.where.not(id: @watched + current_user.films).limit(50).sample(params.fetch(:count, 5).to_i)
    @redis.set(jwt_key(:films), @watched + taken_now.pluck(:id))
    unless taken_now.empty?
      render json: taken_now
    else
      render status: 204
    end
  end

  def watched
    unless current_user.films.empty?
      render json: current_user.films
    else
      render status: 204
    end
  end

  def favorites
    unless current_user.favorite_films.empty?
      render json: current_user.favorite_films.to_json(include: :film)
    else
      render status: 204
    end
  end

  def mark
    unless current_user.films.where(id: params[:id]).present?
      current_user.films << Film.find(params[:id])
    end
    current_user.films_users.find_by(film_id: params[:id]).favorites!
    render status: :ok
  end

  def refresh
    @redis.set(jwt_key(:films),[])
  end

end
