class UsersController < APIBaseController
  before_action :load_user, except: :create
  authorize_resource except: :create
  before_action :authenticate_user, except: :create

  def show
    render json: @user.to_json(except: [:password_digest])
  end

  def update
    @user.update(update_user_params)
    if @user.errors.blank?
      render status: :ok
    else
      render json: @user.errors, status: 250
    end
  end

  def create
    @user = User.create(create_user_params)
    if @user.errors.blank?
      render status: :ok
    else
      render json: @user.errors, status: 250
    end
  end

  def destroy
    @user.delete
  end

  protected

  def load_user
    @user = current_user
  end

  def default_user_fields
    %i[email]
  end

  def update_user_params
    params.required(:user).permit(
      *default_user_fields
    )
  end

  def create_user_params
    params.required(:user).permit(
      *default_user_fields, :password, :username
    )
  end
end
