class BooksController < APIBaseController
  before_action :authenticate_user


  def show
    book = Book.find(params[:id]) #Выбираем книгу из списка
    if current_user.books.where(id: book).present?
      render json: book
    else
      current_user.books << book  #Добавление юзеру в просмотренные
      render json: book
    end
  end

  def index  
    @watched = JSON.parse(@redis.get(jwt_key(:books)))
    taken_now = Book.where.not(id: @watched + current_user.books).limit(50).sample(params.fetch(:count, 5).to_i)
    @redis.set(jwt_key(:books), @watched + taken_now.pluck(:id))
    unless taken_now.empty?
      render json: taken_now
    else
      render status: 204
    end
  end

  def watched
    unless current_user.books.empty?
      render json: current_user.books
    else
      render status: 204
    end
  end

  def favorites
    unless current_user.favorite_books.empty?
      render json: current_user.favorite_books.to_json(include: :book)
    else
      render status: 204
    end
  end

  def mark
    unless current_user.books.where(id: params[:id]).present?
      current_user.books << Book.find(params[:id])
    end
    current_user.books_users.find_by(book_id: params[:id]).favorites!
    render status: :ok
  end

  def refresh
    @redis.set(jwt_key(:books),[])
  end

end
