class CreateJoinTableUsersBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books_users do |t|
      t.belongs_to :book
      t.belongs_to :user
      t.boolean :favorites, default: false
    end
  end
end
