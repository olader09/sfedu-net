class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :name, null: false
      t.string :author, null: false
      t.string :info, null: false
      t.string :img, null: false
      t.string :source, null: false
      t.timestamps
    end
  end
end
