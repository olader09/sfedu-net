class CreateJoinTableUsersFilms < ActiveRecord::Migration[5.1]
  def change
    create_table :films_users do |t|
      t.belongs_to :film
      t.belongs_to :user
      t.boolean :favorites, default: false
    end
  end
end