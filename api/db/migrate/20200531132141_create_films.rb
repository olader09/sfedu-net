class CreateFilms < ActiveRecord::Migration[5.1]
  def change
    create_table :films do |t|
      t.string :name, null: false
      t.integer :pg, default: 0
      t.integer :year, null: false, default: 0
      t.string :genre, null: false
      t.string :actors, null: false
      t.string :director, null: false
      t.string :img, null: false
      t.string :source, null: false
      t.timestamps
    end
  end
end
